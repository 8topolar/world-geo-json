const fetch = require( 'node-fetch' )
const { writeFileSync } = require( 'fs' )

fetch( 'https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_50m_admin_0_countries.geojson' )
  .then( res => res.text() )
  .then( body => {
    const map = JSON.parse( body )

    const countries = map.features
      .filter( el => /country/i.test( el.properties.type ) )
      .filter( el => el.properties.sovereignt === el.properties.admin )
      .map( el => ( {
        type: el.type,
        id: el.properties.iso_a3 === '-99' ? el.properties.adm0_a3 : el.properties.iso_a3,
        properties: {
          name: el.properties.sovereignt,
          name_long: el.properties.name_long,
          en: el.properties.formal_en,
        },
        geometry: el.geometry
      } ) )
    
    const content = '{"type":"FeatureCollection","features":[\n' +
      countries.map( el => JSON.stringify( el ) ).join( ',\n' ) +
      '\n]}'

    writeFileSync( 'countries.geo.json', content )
  } )
